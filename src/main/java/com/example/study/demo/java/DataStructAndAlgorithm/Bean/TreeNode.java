package com.example.study.demo.java.DataStructAndAlgorithm.Bean;

public class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
        TreeNode(int x) { val = x; }
    }